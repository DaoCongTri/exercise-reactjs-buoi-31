import logo from "./logo.svg";
import "./App.css";
import Ex_glasses from "./exercise/ExGlasses";
function App() {
  return (
    <div className="App">
      <img src={logo} className="App-logo" alt="logo" />
      <Ex_glasses />
    </div>
  );
}

export default App;
